import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.stream.Stream;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 15 Dec 2018
 * Create your own annotation. Create class with a few fields, some of
 * which annotate with this annotation. Through reflection print those
 * fields in the class that were annotate by this annotation.
 */
@Application.Service(name = "class_annotation")
public class Application {

    @FieldService(name = "field_I_annotation", flag = true)
    private int i;


    @FieldService(name = "field_D_annotation", flag = false)
    private double d;

    private String s;

    Application(int i, double d, String s) {
        this.i = i;
        this.d = d;
        this.s = s;
    }

    @Init
    private int getI() {
        System.out.println("It's method GetI");
        return i * 100;
    }

    @Init
    public double getD() {
        System.out.println("It's method GetD");
        return d;
    }

    public String getS() {
        return s;
    }

    @Init
    void printSomething() {
        System.out.println("It is method printSomething");
    }

    @Documented
    @Inherited
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface FieldService {
        String name();

        boolean flag();
    }

    @Documented
    @Inherited
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Service {
        String name();

        boolean lazyLoad() default false;
    }

    @Inherited
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Init {
        boolean suppressException() default false;
    }

    public static void main(String[] args) throws NoSuchFieldException, InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        printMethodsNamesWithAnnotation();
        printFieldsNamesWithAnnotation();
        printAnnotationValue();
        invokeMethod();
        setValueIntoField();
    }

    /*
    Task2+. Prints methods in the class that were annotate by some annotation
     */
    private static void printMethodsNamesWithAnnotation() {
        Method[] methods = Application.class.getMethods();
        for (Method m : methods) {
            if (m.isAnnotationPresent(Init.class)) {
                System.out.println(m.getName());
            }
        }
    }

    /*
    Task2. Prints fields in the class that were annotate by some annotation
    */
    private static void printFieldsNamesWithAnnotation() {
        Stream.of(Application.class.getDeclaredFields()).forEach(n -> {
            if (n.isAnnotationPresent(FieldService.class)) {
                System.out.println(n.getName());
            }
        });
    }

    /*
    Task3. Prints annotation value into console
    */
    private static void printAnnotationValue() throws NoSuchFieldException {
        Field[] fields = Application.class.getDeclaredFields();
        for (Field f : fields) {
            if (f.getAnnotation((Class<? extends Annotation>) FieldService.class) != null) {
                System.out.println(f.getAnnotation(FieldService.class).flag());
            }
        }
    }

    /*
    task4. Invokes methods with return type and void type
     */
    private static void invokeMethod() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Method[] methods = Application.class.getDeclaredMethods();
        for (Method m : methods) {
            if (m.getAnnotation(Init.class) != null) {
                m.invoke(new Application(5, 2.0, "Hello world"));
            }
        }
        Method method = Application.class.getDeclaredMethod("getI");
        if (method.getAnnotation(Init.class) != null) {
            System.out.println(method.invoke(new Application(12, 1.0, "oops")));
        }
    }

    /*
    task5. Sets value into field
     */
    private static void setValueIntoField() throws NoSuchFieldException, IllegalAccessException {
        Field field = Application.class.getDeclaredField("i");
        if (field.getAnnotation(FieldService.class) != null) {
            Type type = field.getType();

            field.setInt(new Application(5,2.0, "ooo"), 11);
        }
    }
}

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

/*
Task7. Create your own class that received object of unknown type and
show all information about that Class.
 */
public class MyOwnClass {

    private static void getClassInfo(Object o) {
        StringBuilder sb = new StringBuilder();
        System.out.println("OBJECT INFO");
        Class<?> clazz = o.getClass();
        System.out.println(sb.append("class name: ").append(clazz.getName())
                .append(", superclass name: ").append(clazz.getSuperclass().getName()));

        System.out.println("OBJECT FIELDS");
        Field[] fields = clazz.getDeclaredFields();
        Stream.of(fields).forEach(n -> {
            System.out.println("field name: " + n.getName() +
                    ", type: " + n.getType());
        });

        System.out.println("OBJECT CONSTRUCTORS");
        Constructor[] constructors = clazz.getDeclaredConstructors();
        Stream.of(constructors).forEach(n -> System.out.println("name: " +
                n.getName() + ", parameter types: " + Arrays.toString(n.getParameterTypes())));

        System.out.println("OBJECT METHODS");
        Method[] methods = clazz.getDeclaredMethods();
        Stream.of(methods).forEach(n -> System.out.println("name: " + n.getName() +
                ", parameter types: " + Arrays.toString(n.getParameterTypes()) +
                ", return type: " + n.getReturnType()));

    }

    public static void main(String[] args) {
        getClassInfo(new Application(5, 1.0, "something"));
    }
}
